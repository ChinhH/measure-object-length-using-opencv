from PIL import Image
import numpy as np

data = np.zeros((512, 512, 3), dtype=np.uint8)
data[:, :] = [255, 255, 255] # red patch in upper left
img = Image.fromarray(data, 'RGB')
img.save('my.png')
img.show()