# import the necessary packages
import numpy as np
import argparse
import glob
import cv2
def auto_canny(image, sigma=0.33):
    # compute the median of the single channel pixel intensities
    v = np.median(image)

    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)

    # return the edged image
    return edged

imagePath = '../Data/example.jpg'
# load the image, convert it to grayscale, and blur it slightly
image = cv2.imread(imagePath)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(gray, 50, 255, 1)
blurred = cv2.GaussianBlur(thresh, (3, 3), 0)
tight = cv2.Canny(blurred, 150, 100)
cv2.imshow('123', tight)
cv2.waitKey(0)
cv2.destroyAllWindows()

cdstP = cv2.cvtColor(tight, cv2.COLOR_GRAY2BGR)

linesP = cv2.HoughLinesP(tight, 3, np.pi / 180, 200, None, 150, 25)
if linesP is not None:
    for i in range(0, len(linesP)):
        l = linesP[i][0]
        # print(l[0])
        # print(l[1])
        # print(l[2])
        # print(l[3])
        # deltax = l[0] - l[2]
        # deltay = l[1] - l[3]
        # if deltax < 0:
        #     deltax = deltax*(-1)
        # if deltay < 0:
        #     deltay = deltay*(-1)
        #
        # if deltax < 20:
        #     if 400< l[0] < 800:
        #         cv2.line(cdstP, (l[0], l[1]), (l[2], l[3]), (255, 0, 0), 2, cv2.LINE_AA)
        #
        # elif deltay < 20:
        #     if 400< l[1] <800:
        cv2.line(cdstP, (l[0], l[1]), (l[2], l[3]), (255, 0, 0), 2, cv2.LINE_AA)
    cv2.imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP)
    cv2.waitKey(0)

'''
# apply Canny edge detection using a wide threshold, tight
# threshold, and automatically determined threshold
wide = cv2.Canny(blurred, 10, 200)

auto = auto_canny(blurred)




# gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

kernel = np.ones((2, 2), np.uint8)
d_im = cv2.dilate(thresh, kernel, iterations=1)
# # e_im = cv2.erode(d_im, kernel, iterations=2)
horizontal = d_im
# rows, cols = horizontal.shape
horizontalsize = 100
horizontalStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (int(horizontalsize), 1))
horizontal = cv2.erode(horizontal, horizontalStructure, (-1, -1))
horizontal = cv2.dilate(horizontal, horizontalStructure, (-1, -1))

contours, hier = cv2.findContours(horizontal, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# ymax1=0

# tach nhung o can lay
lstY = []
for c in contours:
    x, y, w, h = cv2.boundingRect(c)

    #    cv2.drawContours(img, contours, 3, (210,110,255), 2)
    #    cnt = contours[4]
    #    cv2.drawContours(img, [cnt], 0, (0,255,0), 3)
    if w >= 10 and w <= 700 and h >= 5 and h <= 50:
        #    cv2.drawContours(img, contours, -1, (210,110,255), 2)

        cv2.rectangle(image, (x, y), (x + w, y + h), (255, 0, 0), 2)

cv2.imshow('123', image)
cv2.waitKey(0)
cv2.destroyAllWindows()
'''
