from PIL import Image
from matplotlib import pyplot as plt
img = Image.open('../Data/9th Jan 2020 (colour mode)/4.bmp')
# img = Image.open('../Data/demo/exxx.jpg')
pixels = img.load()

new_img = Image.new(img.mode, img.size)
pixels_new = new_img.load()


for i in range(new_img.size[0]):
    for j in range(new_img.size[1]):
        r, b, g = pixels[i,j]
        # if r >= 0 and r < 80 and b > 100 and b < 255 and g >= 0 and g < 10:
        if r == 127 and b == 0 and g == 0 :
        # avg = int(round((r + b + g) / 3))
        # pixels_new[i, j] = (avg, avg, avg, 0)
            pixels_new[i,j] = (0, 0, 0, 0)
        else:
            pixels_new[i, j] = (255, 255, 255, 0)
# new_img.show()
plt.figure(0)
plt.imshow(new_img)

plt.figure(1)
plt.imshow(img)
plt.show()