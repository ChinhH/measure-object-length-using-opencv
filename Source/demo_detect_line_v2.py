import cv2
import numpy as np
import matplotlib
from matplotlib.pyplot import imshow
from matplotlib import pyplot as plt

# First, get the gray image and process GaussianBlur.
img = cv2.imread('./demo/01.bmp')
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

for i in range(img.shape[0]):
    for j in range(img.shape[1]):
        if gray[i][j] > 80 and gray[i][j] != 255:
            # print(gray[i][j])
            gray[i][j] = 254
        elif gray[i][j] == 255:
            gray[i][j] = 0

kernel_size = 5
blur_gray = cv2.GaussianBlur(gray,(kernel_size, kernel_size),0)

cv2.imshow("1",blur_gray)
cv2.waitKey(0)

# Second, process edge detection use Canny.
low_threshold = 50
high_threshold = 150
edges = cv2.Canny(blur_gray, low_threshold, high_threshold)
cv2.imshow("2",edges)
cv2.waitKey(0)

# Then, use HoughLinesP to get the lines. You can adjust the parameters for better performance.
rho = 1  # distance resolution in pixels of the Hough grid
theta = np.pi / 180  # angular resolution in radians of the Hough grid
threshold = 15  # minimum number of votes (intersections in Hough grid cell)
min_line_length = 50  # minimum number of pixels making up a line
max_line_gap = 20  # maximum gap in pixels between connectable line segments
line_image = np.copy(img) * 0  # creating a blank to draw lines on

# Run Hough on edge detected image
# Output "lines" is an array containing endpoints of detected line segments
lines = cv2.HoughLinesP(edges, rho, theta, threshold, np.array([]),
                    min_line_length, max_line_gap)

for line in lines:
    for x1,y1,x2,y2 in line:
        cv2.line(line_image,(x1,y1),(x2,y2),(255,0,0),5)

print(line_image)
# Finally, draw the lines on your srcImage.
# Draw the lines on the  image
lines_edges = cv2.addWeighted(img, 0.8, line_image, 1, 0)
cv2.imshow("3",lines_edges)
cv2.waitKey(0)