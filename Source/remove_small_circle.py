import numpy as np
import cv2
import os
nguong = 150
def color_filter(img, r, g, b):
    colors = [b, g, r]
    result = np.zeros(img.shape, dtype=np.uint8)
    for i in range(3):
        result[:, :, i] = np.where(img[:, :, i] < colors[i], 0, 255)
    return result.astype(np.uint8)

imagePath = '../Data/demo'
for f in sorted(os.listdir(imagePath)):
    if (f[-3:] == 'jpg'):
        image = cv2.imread(imagePath + '/' + f)
        # blurred = cv2.GaussianBlur(image, (3, 3), 0)
        newimage = color_filter(image, nguong, nguong, nguong)
        # cv2.imshow('image', newimage)
        # cv2.waitKey(0)
        a = int(image.shape[0] / 3)
        b = int(image.shape[1] / 3)


        for i in range(a):
            for j in range(b):
                newimage[a + i][b + j] = [255, 255, 255]

        for i in range(1):
            for j in range(image.shape[1]):
                newimage[i][j] = [255, 255, 255]
                newimage[image.shape[0]-i-1][j] = [255, 255, 255]
        for i in range(image.shape[0]):
            for j in range(1):
                newimage[i][j] = [255, 255, 255]
                newimage[i][image.shape[1]-1-j] = [255, 255, 255]

        # cv2.imshow('image', newimage)
        # cv2.waitKey(0)

        gray = cv2.cvtColor(newimage, cv2.COLOR_BGR2GRAY)
        contours = cv2.findContours(gray, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
        for contour in contours:
            x, y, w, h = cv2.boundingRect(contour)
            if w <= 60 and h <= 60:# and y < 1500 and x < 990:
                # cv2.rectangle(newimage, (x, y), (x + w, y + h), (0, 255, 0), 2)
                # cv2.imshow('image', newimage)
                # cv2.waitKey(0)

                for i in range(w):
                    for j in range(h):
                        newimage[y+j][x+i] = [255, 255, 255]
                # cv2.imshow('image', newimage)
                # cv2.waitKey(0)

        # gray2 = cv2.cvtColor(newimage, cv2.COLOR_BGR2GRAY)
        # contoursl = cv2.findContours(gray2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
        # for contourl in contoursl:
        #     cv2.drawContours(newimage, contourl, -1, (0, 0, 0), 1)
        gray2 = cv2.cvtColor(newimage, cv2.COLOR_BGR2GRAY)
        contours2 = cv2.findContours(gray2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
        for contour2 in contours2:
            x, y, w, h = cv2.boundingRect(contour2)
            if w <= 60 and h <= 60:
                for i in range(w):
                    for j in range(h):
                        # print(i)
                        # print(j)
                        newimage[y+j][x+i] = [255, 255, 255]
        gray3 = cv2.cvtColor(newimage, cv2.COLOR_BGR2GRAY)
        contours3 = cv2.findContours(gray3, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
        """
        dst = cv2.Canny(newimage, 50, 100, None, 3)
        # cv2.imshow("",dst)
        # cv2.waitKey(0)

        cdstP = cv2.cvtColor(dst, cv2.COLOR_GRAY2BGR)
        linesP = cv2.HoughLinesP(dst, 1, np.pi / 180, 50, None, 200, 100)
        print(linesP)
        if linesP is not None:
            for i in range(0, len(linesP)):
                l = linesP[i][0]
                cv2.line(cdstP, (l[0], l[1]), (l[2], l[3]), (255, 0, 0), 3, cv2.LINE_AA)
            cv2.imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP)

            cv2.waitKey(0)
"""
#
# """

        for contour3 in contours3:
            x, y, w, h = cv2.boundingRect(contour3)
            if w <= 60 and h <= 60:
                for i in range(w):
                    for j in range(h):
                        # print(i)
                        # print(j)
                        newimage[y + j][x + i] = [255, 255, 255]
            elif w < 1040:
                if y > 900 and w == 796:
                    rate = 796
                    cus = 100
                elif y > 900 and w == 480:
                    rate = 480
                    cus = 30
                else:
                    rate = 798
                    cus = 100

                if w > h:
                    lus = round(w*cus/rate,2)
                else:
                    lus = round(h * cus/rate, 2)
            # else:
                test = str(lus) + "um"
                cv2.rectangle(newimage, (x, y), (x + w, y + h), (0, 255, 0), 2)
                cv2.putText(newimage, test, (x, y - 2), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 1, lineType=cv2.LINE_AA)
                # if w == 796:
                # print('-----------------')
                # print(w)
                # print(y)
                # print('-----------------')
                # cv2.imshow('new_image', newimage)
                # cv2.imshow('orgin_image', image)
                # cv2.waitKey(0)
        cv2.imshow('orgin_image', image)
        cv2.imshow('new_image', newimage)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
#
# """
