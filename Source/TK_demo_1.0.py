# Utilities
import cv2
import numpy as np
import os
# from scipy.ndimage import rotate
# For GUI
import tkinter as tk
from tkinter import filedialog
# from tkinter.scrolledtext import ScrolledText
from PIL import Image, ImageTk


def color_filter(img, r, g, b):
    colors = [b, g, r]
    result = np.zeros(img.shape, dtype=np.uint8)
    for i in range(3):
        result[:, :, i] = np.where(img[:, :, i] < colors[i], 0, 255)
    return result.astype(np.uint8)

def process_img(image,nguong):
    ""
    newimage = color_filter(image, nguong, nguong, nguong)
    a = int(image.shape[0] / 3)
    b = int(image.shape[1] / 3)

    for i in range(a):
        for j in range(b):
            newimage[a + i][b + j] = [255, 255, 255]

    for i in range(1):
        for j in range(image.shape[1]):
            newimage[i][j] = [255, 255, 255]
            newimage[image.shape[0] - i - 1][j] = [255, 255, 255]

    for i in range(image.shape[0]):
        for j in range(1):
            newimage[i][j] = [255, 255, 255]
            newimage[i][image.shape[1] - 1 - j] = [255, 255, 255]

    gray = cv2.cvtColor(newimage, cv2.COLOR_BGR2GRAY)
    contours = cv2.findContours(gray, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        if w <= 60 and h <= 60:  # and y < 1500 and x < 990:
            for i in range(w):
                for j in range(h):
                    newimage[y + j][x + i] = [255, 255, 255]

    gray2 = cv2.cvtColor(newimage, cv2.COLOR_BGR2GRAY)
    contours2 = cv2.findContours(gray2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    for contour2 in contours2:
        x, y, w, h = cv2.boundingRect(contour2)
        if w <= 60 and h <= 60:
            for i in range(w):
                for j in range(h):
                    # print(i)
                    # print(j)
                    newimage[y + j][x + i] = [255, 255, 255]
    gray3 = cv2.cvtColor(newimage, cv2.COLOR_BGR2GRAY)
    contours3 = cv2.findContours(gray3, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    for contour3 in contours3:
        x, y, w, h = cv2.boundingRect(contour3)
        if w <= 60 and h <= 60:
            for i in range(w):
                for j in range(h):
                    newimage[y + j][x + i] = [255, 255, 255]
        elif w < 1040:
            if y > 900 and w == 796:
                rate = 796
                cus = 100
            elif y > 900 and w == 480:
                rate = 480
                cus = 30
            else:
                rate = 638
                cus = 100
            print('w: {} and y: {}'.format(w,y))
            print('-------------------')


            if w > h:
                lus = round(w * cus / rate, 2)
            else:
                lus = round(h * cus / rate, 2)
            # else:
            test = str(lus) + "um"
            cv2.rectangle(newimage, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.putText(newimage, test, (x, y - 2), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 1, lineType=cv2.LINE_AA)

    return newimage

def getimg():
    img_path = filedialog.askopenfilename()
    img = cv2.imread(img_path)
    r = 720 / img.shape[0]
    dim = (int(img.shape[1] * r), 720)
    resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)

    placeholder = cv2.cvtColor(resized.copy(), cv2.COLOR_BGR2RGB)
    placeholder = Image.fromarray(placeholder)
    placeholder = ImageTk.PhotoImage(placeholder)
    panel.configure(image=placeholder)
    panel.image = placeholder
    root.update_idletasks()
    output = process_img(img,130)

    resized_out = cv2.resize(output, dim, interpolation=cv2.INTER_AREA)
    placeholder2 = cv2.cvtColor(resized_out.copy(), cv2.COLOR_BGR2RGB)
    placeholder2 = Image.fromarray(placeholder2)
    placeholder2 = ImageTk.PhotoImage(placeholder2)
    panel2.configure(image=placeholder2)
    panel2.image = placeholder2
    root.update_idletasks()


if __name__ == '__main__':
    root = tk.Tk()
    root.title('Phần mềm tính khoảng chiều dài vết nứt trên bề mặt vật thể')
    root.geometry('1920x1080')

    # ======= Button Main ===========================#
    btn_img = tk.Button(root, text='GET-IMAGE', command=getimg, font=("Times New Roman", 13, "bold"), foreground="blue")
    btn_img.place(relx=.15, rely=.05, height=40, width=150, anchor='c')

    # ==== image_input ==================================#
    placeholder = cv2.imread('./placeholder2.png')
    placeholder = cv2.cvtColor(placeholder, cv2.COLOR_BGR2RGB)
    placeholder = Image.fromarray(placeholder)
    placeholder = ImageTk.PhotoImage(placeholder)
    panel = tk.Label(image=placeholder)
    panel.image = placeholder
    panel.place(relx=.025, rely=.15)

    # ==== image_output ==================================#
    placeholder2 = cv2.imread('./placeholder2.png')
    placeholder2 = cv2.cvtColor(placeholder2, cv2.COLOR_BGR2RGB)
    placeholder2 = Image.fromarray(placeholder2)
    placeholder2 = ImageTk.PhotoImage(placeholder2)
    panel2= tk.Label(image=placeholder2)
    panel2.image = placeholder2
    panel2.place(relx=.5, rely=.15)

    # ======= Button Exit ===========================#
    exitbt = tk.Button(root, text='Exit', command=exit,font=("Times New Roman", 13, "bold"),foreground="blue")
    exitbt.place(relx=.3, rely=.05, height=40, width=80, anchor='c')

    # kick off the GUI
    root.mainloop()