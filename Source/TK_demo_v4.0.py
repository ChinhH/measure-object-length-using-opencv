# Utilities
import math
import cv2
import numpy as np
import tkinter as tk
from tkinter import filedialog
from PIL import Image, ImageTk

def del_4goc(img):
    a = int(img.shape[0] / 7)
    b = int(img.shape[1] / 7)
    result = np.zeros(img.shape, dtype=np.uint8)
    for i in range(3):
        result[:, :, i] = img[:, :, i]
    for i in range(3 * a):
        for j in range(3 * b):
            result[i][j] = [100, 100, 100]
            result[i][4 * b + j] = [100, 100, 100]

            if 4 * a + i < 950:
                result[4 * a + i][j] = [100, 100, 100]
                result[4 * a + i][4 * b + j] = [100, 100, 100]
    return result

def process_img(img):
    ""
    img2 = del_4goc(img)
    # smooth the image with alternative closing and opening
    # with an enlarging kernel
    morph = img2.copy()

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 1))
    morph = cv2.morphologyEx(morph, cv2.MORPH_CLOSE, kernel)
    morph = cv2.morphologyEx(morph, cv2.MORPH_OPEN, kernel)

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))

    # take morphological gradient
    gradient_image = cv2.morphologyEx(morph, cv2.MORPH_GRADIENT, kernel)

    # split the gradient image into channels
    image_channels = np.split(np.asarray(gradient_image), 3, axis=2)

    channel_height, channel_width, _ = image_channels[0].shape

    # apply Otsu threshold to each channel
    for i in range(0, 3):
        _, image_channels[i] = cv2.threshold(~image_channels[i], 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY)
        image_channels[i] = np.reshape(image_channels[i], newshape=(channel_height, channel_width, 1))

    # merge the channels
    image_channels = np.concatenate((image_channels[0], image_channels[1], image_channels[2]), axis=2)

    gray = cv2.cvtColor(image_channels, cv2.COLOR_BGR2GRAY)

    kernel_size = 5
    blur_gray = cv2.GaussianBlur(gray, (kernel_size, kernel_size), 0)

    # cv2.imshow("1",blur_gray)
    # cv2.waitKey(0)

    # Second, process edge detection use Canny.
    low_threshold = 20
    high_threshold = 160
    edges = cv2.Canny(blur_gray, low_threshold, high_threshold)

    # Then, use HoughLinesP to get the lines. You can adjust the parameters for better performance.
    rho = 2  # distance resolution in pixels of the Hough grid
    theta = np.pi / 180  # angular resolution in radians of the Hough grid
    threshold = 20  # minimum number of votes (intersections in Hough grid cell)
    min_line_length = 20  # minimum number of pixels making up a line
    max_line_gap = 50  # maximum gap in pixels between connectable line segments
    line_image = np.copy(img2) * 0  # creating a blank to draw lines on

    # Run Hough on edge detected image
    # Output "lines" is an array containing endpoints of detected line segments
    lines = cv2.HoughLinesP(edges, rho, theta, threshold, np.array([]),
                            min_line_length, max_line_gap)

    for line in lines:
        for x1, y1, x2, y2 in line:
            if y1 < 900 and y2 < 900:
                deltaX = x2 - x1
                if y2 > y1:
                    deltaY = y2 - y1
                else:
                    deltaY = y1 - y2
                if x2 == x1 or y2 == y1:
                    cv2.line(line_image, (x1, y1), (x2, y2), (255, 255, 255), 5)
                else:
                    phi_oC = math.degrees(math.atan(deltaY / deltaX))
                    if 0 <= phi_oC <= 10 or 85 <= phi_oC <= 95:
                        cv2.line(line_image, (x1, y1), (x2, y2), (255, 255, 255), 5)

    # Finally, draw the lines on your srcImage.
    # Draw the lines on the  image
    lines_edges = cv2.addWeighted(img2, 1, line_image, 1, 0)

    mask = cv2.cvtColor(lines_edges, cv2.COLOR_BGR2GRAY)

    for i in range(img2.shape[0]):
        for j in range(img2.shape[1]):
            if mask[i][j] != 255:
                mask[i][j] = 0

    contours3 = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    xmin = 800
    xmax = 0
    ymin = 800
    ymax = 0
    bit = 289
    um = 10
    for contour3 in contours3:
        x, y, w, h = cv2.boundingRect(contour3)
        if w >= 192 or h >= 192:
            print('x', x)
            print('y', y)
            print('w', w)
            print('h', h)
            # input()
            if y > 900 and w == 796:
                bit = 796
                um = 100
                cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
                cv2.putText(img, "100.0um", (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2,
                            lineType=cv2.LINE_AA)
            elif y > 900 and w == 480:
                bit = 480
                um = 30
                cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
                cv2.putText(img, "30.0um", (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2,
                            lineType=cv2.LINE_AA)
            elif y > 900 and w == 291:
                bit = 291
                um = 10
                cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
                cv2.putText(img, "10.0um", (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2,
                            lineType=cv2.LINE_AA)
            elif y > 900 and w == 289:
                bit = 289
                um = 10
                cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
                cv2.putText(img, "10.0um", (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2,
                            lineType=cv2.LINE_AA)

            print('w: {} and y: {}'.format(w, y))
            print('-------------------')

            if y < 900:
                if x < xmin:
                    xmin = x
                if y < ymin:
                    ymin = y
                if x + w > xmax:
                    xmax = x + w
                if y + h > ymax:
                    ymax = y + h
    print('xmin', xmin)
    print('xmax', xmax)
    print('ymin', ymin)
    print('ymax', ymax)

    if xmax - xmin > ymax - ymin:
        lus = round((xmax - xmin + 10) * um / bit, 2)
        test = str(lus) + "um"
        cv2.rectangle(img, (xmin - 5, ymin), (xmax + 5, ymax), (0, 255, 0), 2)
        cv2.putText(img, test, (xmin, ymin - 5), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, lineType=cv2.LINE_AA)
    else:
        lus = round((ymax - ymin + 10) * um / bit, 2)
        test = str(lus) + "um"
        cv2.rectangle(img, (xmin, ymin - 5), (xmax, ymax + 5), (0, 255, 0), 2)
        cv2.putText(img, test, (xmin, ymin - 5), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, lineType=cv2.LINE_AA)
    # # save the denoised image
    # cv2.imwrite('output.jpg', img)
    return img

def getimg():
    global img
    global dim
    global Width
    global Height

    img_path = filedialog.askopenfilename()
    img = cv2.imread(img_path)

    r = (Width/2.67) / img.shape[0]
    dim = (int(img.shape[1] * r), int((Width/2.67)))
    resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    print(dim)
    placeholder = cv2.cvtColor(resized.copy(), cv2.COLOR_BGR2RGB)
    placeholder = Image.fromarray(placeholder)
    placeholder = ImageTk.PhotoImage(placeholder)
    panel.configure(image=placeholder)
    panel.image = placeholder
    root.update_idletasks()
    runT()

def runT():
    global img
    global dim

    # th = int(entry_threshold.get())

    output = process_img(img)
    resized_out = cv2.resize(output, dim, interpolation=cv2.INTER_AREA)
    placeholder2 = cv2.cvtColor(resized_out.copy(), cv2.COLOR_BGR2RGB)
    placeholder2 = Image.fromarray(placeholder2)
    placeholder2 = ImageTk.PhotoImage(placeholder2)
    panel2.configure(image=placeholder2)
    panel2.image = placeholder2
    root.update_idletasks()

if __name__ == '__main__':
    root = tk.Tk()
    root.title('Phần mềm tính chiều dài vết nứt trên bề mặt vật thể')
    from win32api import GetSystemMetrics

    global Width
    global Height
    Width= GetSystemMetrics(0)
    Height = GetSystemMetrics(1)

    root.geometry(str(Width)+'x'+str(Height))

    # ======= Button Main ===========================#
    btn_img = tk.Button(root, text='GET-IMAGE', command=getimg, font=("Times New Roman", 13, "bold"), foreground="blue")
    btn_img.place(relx=.15, rely=.05, height=Height/27, width=Width/12.8, anchor='c')

    # ==== image_input ==================================#
    data = np.zeros((200, 200, 3), dtype=np.uint8)
    data[:, :] = [255, 255, 255]  # red patch in upper left
    placeholder = Image.fromarray(data, 'RGB')
    placeholder = ImageTk.PhotoImage(placeholder)
    panel = tk.Label(image=placeholder)
    panel.image = placeholder
    panel.place(relx=.025, rely=.15)

    # ==== image_output ==================================#
    placeholder2 = Image.fromarray(data, 'RGB')
    placeholder2 = ImageTk.PhotoImage(placeholder2)
    panel2= tk.Label(image=placeholder2)
    panel2.image = placeholder2
    panel2.place(relx=.5, rely=.15)

    # # ======= entry threshold ==========================#
    # lb_entry_threshold = tk.Label(root, text='Entry_threshold \n (120 - 180) ', font=("Times New Roman", 13, "bold"),
    #                               foreground="blue")
    # lb_entry_threshold.place(relx=0.38, rely=0.05, anchor='c')

    entry_threshold = tk.Entry(root,font=("Times New Roman", 13, "bold"),foreground="blue")
    # entry_threshold.place(relx=0.45, rely=0.05, height=Height/27, width=Width/24, anchor='c')
    entry_threshold.insert(0, 130)

    # # ============ run ====================================#
    # bnt_run = tk.Button(root, text="RUN", command=runT, font=("Times New Roman", 13, "bold"),
    #                     foreground="blue")
    # bnt_run.place(relx=.55, rely=.05, height=Height/27, width=Width/12.8, anchor='c')


    # ======= Button Exit ===========================#
    exitbt = tk.Button(root, text='Exit', command=exit,font=("Times New Roman", 13, "bold"),foreground="blue")
    exitbt.place(relx=.25, rely=.05, height=Height/27, width=Width/24, anchor='c')

    # kick off the GUI
    root.mainloop()