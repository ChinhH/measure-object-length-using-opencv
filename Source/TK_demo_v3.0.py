# Utilities
import cv2
import numpy as np
import tkinter as tk
from tkinter import filedialog

from PIL import Image, ImageTk

def color_filter(img, r, g, b):
    colors = [b, g, r]
    result = np.zeros(img.shape, dtype=np.uint8)
    for i in range(3):
        result[:, :, i] = np.where(img[:, :, i] < colors[i], 0, 255)
    return result.astype(np.uint8)

def process_img(image,nguong):
    ""
    newimage = color_filter(image, nguong, nguong, nguong)

    a = int(image.shape[0] / 5)
    b = int(image.shape[1] / 5)
    for i in range(2*a):
        for j in range(2*b):
            newimage[i][j] = [0, 0, 0]
            newimage[i][3 * b + j] = [0, 0, 0]
            newimage[3*a+i][3*b+j] = [0, 0, 0]
            if 3*a+i < 900:
                newimage[3 * a + i][j] = [0, 0, 0]

    gray = cv2.cvtColor(newimage, cv2.COLOR_BGR2GRAY)
    contours = cv2.findContours(gray, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        if w <= 20 and h <= 20:  # and y < 1500 and x < 990:
            for i in range(w):
                for j in range(h):
                    newimage[y + j][x + i] = [0, 0, 0]
    ret, newimage = cv2.threshold(newimage, 200, 255, 1)

    gray2 = cv2.cvtColor(newimage, cv2.COLOR_BGR2GRAY)
    contours2 = cv2.findContours(gray2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    for contour2 in contours2:
        x, y, w, h = cv2.boundingRect(contour2)
        if w <= 60 and h <= 60:
            for i in range(w):
                for j in range(h):
                    newimage[y + j][x + i] = [255, 255, 255]

    gray2 = cv2.cvtColor(newimage, cv2.COLOR_BGR2GRAY)
    contours2 = cv2.findContours(gray2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    for contour2 in contours2:
        x, y, w, h = cv2.boundingRect(contour2)
        if w <= 60 and h <= 60:
            for i in range(w):
                for j in range(h):
                    newimage[y + j][x + i] = [255, 255, 255]

    gray3 = cv2.cvtColor(newimage, cv2.COLOR_BGR2GRAY)
    contours3 = cv2.findContours(gray3, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    xmin = 500
    xmax = 0
    ymin = 500
    ymax = 0
    for contour3 in contours3:
        x, y, w, h = cv2.boundingRect(contour3)
        if w <= 60 and h <= 60:
            ''
        elif w > h and w - h < 10:
            for i in range(w):
                for j in range(h):
                    newimage[y + j][x + i] = [255, 255, 255]
        elif h > w and h - w < 10:
            for i in range(w):
                for j in range(h):
                    newimage[y + j][x + i] = [255, 255, 255]
        elif w < 1040:
            if y > 900 and w == 796:
                rate = 796
                cus = 100
            elif y > 900 and w == 480:
                rate = 480
                cus = 30
            elif y > 900 and w == 291:
                rate = 291
                cus = 10
                cv2.rectangle(newimage, (x, y), (x+w, y+h), (0, 255, 0), 2)
                cv2.putText(newimage, "10.0um", (x, y - 2), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 1,
                            lineType=cv2.LINE_AA)
            else:
                rate = 289
                cus = 10
            print('w: {} and y: {}'.format(w,y))
            print('-------------------')

            if y < 900:
                if x < xmin:
                    xmin = x
                if y < ymin:
                    ymin = y
                if x + w > xmax:
                    xmax = x + w
                if y + h > ymax:
                    ymax = y + h

    if xmax - xmin > ymax - ymin:
        lus = round((xmax - xmin) * cus / rate, 2)
    else:
        lus = round((ymax - ymin) * cus / rate, 2)
    test = str(lus) + "um"

    cv2.rectangle(newimage, (xmin, ymin), (xmax, ymax), (0, 255, 0), 2)
    cv2.putText(newimage, test, (xmin, ymin - 2), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 1, lineType=cv2.LINE_AA)
    return newimage

def getimg():
    global img
    global dim
    global Width
    global Height

    img_path = filedialog.askopenfilename()
    img = cv2.imread(img_path)

    r = (Width/2.67) / img.shape[0]
    dim = (int(img.shape[1] * r), int((Width/2.67)))
    resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    print(dim)
    placeholder = cv2.cvtColor(resized.copy(), cv2.COLOR_BGR2RGB)
    placeholder = Image.fromarray(placeholder)
    placeholder = ImageTk.PhotoImage(placeholder)
    panel.configure(image=placeholder)
    panel.image = placeholder
    root.update_idletasks()
    runT()

def runT():
    global img
    global dim

    th = int(entry_threshold.get())

    output = process_img(img,th)
    resized_out = cv2.resize(output, dim, interpolation=cv2.INTER_AREA)
    placeholder2 = cv2.cvtColor(resized_out.copy(), cv2.COLOR_BGR2RGB)
    placeholder2 = Image.fromarray(placeholder2)
    placeholder2 = ImageTk.PhotoImage(placeholder2)
    panel2.configure(image=placeholder2)
    panel2.image = placeholder2
    root.update_idletasks()

if __name__ == '__main__':
    root = tk.Tk()
    root.title('Phần mềm tính khoảng chiều dài vết nứt trên bề mặt vật thể')
    from win32api import GetSystemMetrics

    global Width
    global Height
    Width= GetSystemMetrics(0)
    Height = GetSystemMetrics(1)

    root.geometry(str(Width)+'x'+str(Height))

    # ======= Button Main ===========================#
    btn_img = tk.Button(root, text='GET-IMAGE', command=getimg, font=("Times New Roman", 13, "bold"), foreground="blue")
    btn_img.place(relx=.15, rely=.05, height=Height/27, width=Width/12.8, anchor='c')

    # ==== image_input ==================================#
    data = np.zeros((200, 200, 3), dtype=np.uint8)
    data[:, :] = [255, 255, 255]  # red patch in upper left
    placeholder = Image.fromarray(data, 'RGB')
    placeholder = ImageTk.PhotoImage(placeholder)
    panel = tk.Label(image=placeholder)
    panel.image = placeholder
    panel.place(relx=.025, rely=.15)

    # ==== image_output ==================================#
    placeholder2 = Image.fromarray(data, 'RGB')
    placeholder2 = ImageTk.PhotoImage(placeholder2)
    panel2= tk.Label(image=placeholder2)
    panel2.image = placeholder2
    panel2.place(relx=.5, rely=.15)

    # # ======= entry threshold ==========================#
    # lb_entry_threshold = tk.Label(root, text='Entry_threshold \n (120 - 180) ', font=("Times New Roman", 13, "bold"),
    #                               foreground="blue")
    # lb_entry_threshold.place(relx=0.38, rely=0.05, anchor='c')

    entry_threshold = tk.Entry(root,font=("Times New Roman", 13, "bold"),foreground="blue")
    # entry_threshold.place(relx=0.45, rely=0.05, height=Height/27, width=Width/24, anchor='c')
    entry_threshold.insert(0, 130)

    # # ============ run ====================================#
    # bnt_run = tk.Button(root, text="RUN", command=runT, font=("Times New Roman", 13, "bold"),
    #                     foreground="blue")
    # bnt_run.place(relx=.55, rely=.05, height=Height/27, width=Width/12.8, anchor='c')


    # ======= Button Exit ===========================#
    exitbt = tk.Button(root, text='Exit', command=exit,font=("Times New Roman", 13, "bold"),foreground="blue")
    exitbt.place(relx=.25, rely=.05, height=Height/27, width=Width/24, anchor='c')

    # kick off the GUI
    root.mainloop()