from PIL import Image
import cv2
import numpy as np
import os
from matplotlib import pyplot as plt


def color_filter2(img, rt, gt, bt):
    result = np.zeros(img.shape, dtype=np.uint8)
    for i in range(result.shape[0]):
        for j in range(result.shape[1]):
            r = img[i][j][2]
            g = img[i][j][1]
            b = img[i][j][0]
            # if r > rt and r < rt+50 and b > bt and b < bt+50 and g > gt and g < gt+50:
            if r == rt and b == bt and g == gt:
                result[i][j] = [0, 0, 0]
            else:
                result[i][j] = [255, 255, 255]
    return result.astype(np.uint8)


ig = cv2.imread('../Data/demo/aa.bmp')
# newimage = color_filter2(ig, 200, 30, 30)
newimage = color_filter2(ig, 0, 0, 0)
cv2.imshow("",newimage)
cv2.waitKey(0)
