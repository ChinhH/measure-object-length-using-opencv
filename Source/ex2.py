import numpy as np
import cv2
import os

def color_filter(img, r, g, b):
    colors = [b, g, r]
    result = np.zeros(img.shape, dtype=np.uint8)
    for i in range(3):
        result[:, :, i] = np.where(img[:, :, i] < colors[i], 0, 255)
    return result.astype(np.uint8)

imagePath = '../Data/demo'
for f in sorted(os.listdir(imagePath)):
    if (f[-3:] == 'jpg'):
        image = cv2.imread(imagePath + '/' + f)
        # blurred = cv2.GaussianBlur(image, (3, 3), 0)
        newimage = color_filter(image, 110, 110, 110)

        gray = cv2.cvtColor(newimage, cv2.COLOR_BGR2GRAY)
        contours = cv2.findContours(gray, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
        for contour in contours:
            x, y, w, h = cv2.boundingRect(contour)
            if ((w >= 50 and w < 700 and h < 30) or (h >= 50 and h <700 and w < 30)) and y < 900:
                cv2.rectangle(newimage, (x-5, y-5), (x + w, y + h), (0, 255, 0), 1)
        cv2.imshow('image', newimage)
        cv2.waitKey(0)
        cv2.destroyAllWindows()


