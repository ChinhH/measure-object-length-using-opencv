# Python program to illustrate HoughLine
# method for line detection
import cv2
import numpy as np
from matplotlib import pyplot as plt

class Demo_usingHough_V01:
    def __init__(self, imgpath):
        self.imgpath = imgpath
    def processing_Hough(self):
        img = cv2.imread(self.imgpath)
        # Convert the img to grayscale
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # print(gray.shape)

        for i in range(img.shape[0]):
            for j in range(img.shape[1]):
                if gray[i][j] > 80 and gray[i][j] != 255:
                    # print(gray[i][j])
                    gray[i][j] = 254
                elif gray[i][j] == 255:
                    gray[i][j] = 0

        # plt.imshow(gray)
        # plt.show()
        # Setup SimpleBlobDetector parameters.
        params = cv2.SimpleBlobDetector_Params()

        # Change thresholds
        params.minThreshold = 10;
        params.maxThreshold = 150;

        # Filter by Area.
        params.filterByArea = True
        params.minArea = 20

        # Filter by Circularity
        params.filterByCircularity = True
        params.minCircularity = 0.1

        # Filter by Convexity
        params.filterByConvexity = True
        params.minConvexity = 0.9

        # Filter by Inertia
        params.filterByInertia = True
        params.minInertiaRatio = 0.01

        # Create a detector with the parameters
        ver = (cv2.__version__).split('.')
        if int(ver[0]) < 3:
            detector = cv2.SimpleBlobDetector(params)
        else:
            detector = cv2.SimpleBlobDetector_create(params)
        # Set up the detector with default parameters.
        # detector = cv2.SimpleBlobDetector()
        print('2',detector)
        # Detect blobs.
        keypoints = detector.detect(gray)
        print('1',keypoints)
        # Draw detected blobs as red circles.
        # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
        for i in range(len(keypoints)):
            im_with_keypoints = cv2.circle(gray, (np.int(keypoints[i].pt[0]), np.int(keypoints[i].pt[1])),
                            radius=np.int(keypoints[i].size), color=(255), thickness=-1)

        # im_with_keypoints = cv2.drawKeypoints(gray, keypoints, np.array([]), (0, 0, 255),
        #                                       cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        # Show keypoints
        cv2.imshow("Keypoints", im_with_keypoints)
        cv2.waitKey(0)

        # Apply edge detection method on the image
        edges = cv2.Canny(im_with_keypoints, 50, 150, apertureSize=3)

        cv2.imshow("Source", edges)
        cv2.waitKey(0)
        # This returns an array of r and theta values
        lines = cv2.HoughLines(edges, 1, np.pi / 180, 100)
        print(lines)
        # The below for loop runs till r and theta values
        # are in the range of the 2d array
        for r, theta in lines[0]:
            # Stores the value of cos(theta) in a
            a = np.cos(theta)

            # Stores the value of sin(theta) in b
            b = np.sin(theta)

            # x0 stores the value rcos(theta)
            x0 = a * r

            # y0 stores the value rsin(theta)
            y0 = b * r

            # x1 stores the rounded off value of (rcos(theta)-1000sin(theta))
            x1 = int(x0 + 1000 * (-b))

            # y1 stores the rounded off value of (rsin(theta)+1000cos(theta))
            y1 = int(y0 + 1000 * (a))

            # x2 stores the rounded off value of (rcos(theta)+1000sin(theta))
            x2 = int(x0 - 1000 * (-b))

            # y2 stores the rounded off value of (rsin(theta)-1000cos(theta))
            y2 = int(y0 - 1000 * (a))

            # cv2.line draws a line in img from the point(x1,y1) to (x2,y2).
            # (0,0,255) denotes the colour of the line to be
            # drawn. In this case, it is red.
            cv2.line(im_with_keypoints, (x1, y1), (x2, y2), (0, 0, 255), 2)

        # All the changes made in the input image are finally
        # written on a new image houghlines.jpg
        cv2.imshow("Source", im_with_keypoints)
        cv2.waitKey(0)



if __name__ == '__main__':
    # A = Demo_usingHough_V01('C:/Users/VBPO-HC/Desktop/blob_detection.jpg')
    # A.processing_Hough()
    B = Demo_usingHough_V01('../Data/4th time crack healing_after annealing/demo/01.bmp')
    B.processing_Hough()
# cv2.imwrite('linesDetected.jpg', img)