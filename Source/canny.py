# import the necessary packages
import numpy as np
import argparse
import glob
import cv2

def auto_canny(image, sigma=0.33):
    # compute the median of the single channel pixel intensities
    v = np.median(image)

    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)

    # return the edged image
    return edged

imagePath = '../Data/example.jpg'
# load the image, convert it to grayscale, and blur it slightly
image = cv2.imread(imagePath)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(gray, (3, 3), 0)

# apply Canny edge detection using a wide threshold, tight
# threshold, and automatically determined threshold
wide = cv2.Canny(blurred, 10, 200)
tight = cv2.Canny(blurred, 225, 250)
auto = auto_canny(blurred)

def gaussian_kernel(size, sigma=1):
    size = int(size) // 2
    x, y = np.mgrid[-size:size+1, -size:size+1]
    normal = 1 / (2.0 * np.pi * sigma**2)
    g =  np.exp(-((x**2 + y**2) / (2.0*sigma**2))) * normal
    return g


# # show the images
# cv2.imshow("Original", image)
# cv2.imshow("wide", wide)
# cv2.imshow("tight", tight)
# cv2.imshow("auto", auto)
# # # cv2.imshow("Edges", np.hstack([wide, tight, auto]))
# cv2.waitKey(0)

cdstP = cv2.cvtColor(tight, cv2.COLOR_GRAY2BGR)

linesP = cv2.HoughLinesP(tight, 1, np.pi / 180, 150, None, 200, 50)
if linesP is not None:
    for i in range(0, len(linesP)):
        l = linesP[i][0]
        print(l[0])
        print(l[1])
        print(l[2])
        print(l[3])
        deltax = l[0] - l[2]
        deltay = l[1] - l[3]
        if deltax < 0:
            deltax = deltax*(-1)
        if deltay < 0:
            deltay = deltay*(-1)

        if deltax < 20:
            if 400< l[0] < 800:
                cv2.line(cdstP, (l[0], l[1]), (l[2], l[3]), (255, 0, 0), 2, cv2.LINE_AA)

        elif deltay < 20:
            if 400< l[1] <800:
                cv2.line(cdstP, (l[0], l[1]), (l[2], l[3]), (255, 0, 0), 2, cv2.LINE_AA)
    cv2.imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP)
    cv2.waitKey(0)